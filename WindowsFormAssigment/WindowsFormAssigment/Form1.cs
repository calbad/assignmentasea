﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormAssigment
{
    public partial class Form1 : Form
    {

//***JAVA to C# Fractal Conversion, with functionality to save an image and save state***\\
                        //***Code edited by Callum Baddeley***\\
        
        /* 
 * @(#)fractal.java - Fractal Program Mandelbrot Set 
 * www.eckhard-roessel.de
 * Copyright (c) Eckhard Roessel. All Rights Reserved.
 * 06/30/2000 - 03/29/2000
 */
/*
    import java.awt.*;
    import java.applet.*;
    import java.awt.event.*;
 */
/** 
 * @version 1.1
 * @author Eckhard Roessel
 * @modified 03/29/2001
 * @modified 08/16/2006 tweaked by Duncan Mullier 8/2006
 */
 
           public Form1()
        {
            InitializeComponent();
            init(); //CB Added to call the method
            start(); //CB Added to call the method

        }
	
	class HSB
	{//djm added, it makes it simpler to have this code in here than in the C#
		public float rChan,gChan,bChan;
		public HSB()
		{
			rChan=gChan=bChan=0;
		}
		public void fromHSB(float h,float s,float b)
        {
            float red = b;
            float green = b;
            float blue = b;
            if (s != 0)
            {
                float max = b;
                float dif = b * s / 255f;
                float min = b - dif;

                float h2 = h * 360f / 255f;

                if (h2 < 60f)
                {
                    red = max;
                    green = h2 * dif / 60f + min;
                    blue = min;
                }
                else if (h2 < 120f)
                {
                    red = -(h2 - 120f) * dif / 60f + min;
                    green = max;
                    blue = min;
                }
                else if (h2 < 180f)
                {
                    red = min;
                    green = max;
                    blue = (h2 - 120f) * dif / 60f + min;
                }
                else if (h2 < 240f)
                {
                    red = min;
                    green = -(h2 - 240f) * dif / 60f + min;
                    blue = max;
                }
                else if (h2 < 300f)
                {
                    red = (h2 - 240f) * dif / 60f + min;
                    green = min;
                    blue = max;
                }
                else if (h2 <= 360f)
                {
                    red = max;
                    green = min;
                    blue = -(h2 - 360f) * dif / 60 + min;
                }
                else
                {
                    red = 0;
                    green = 0;
                    blue = 0;
                }
            }

           rChan = (float) Math.Round(Math.Min(Math.Max(red, 0), 255)); //CB Changed round to Round, min to Min, max to Max. added (float)
           gChan = (float) Math.Round(Math.Min(Math.Max(green, 0), 255)); //CB Changed round to Round, min to Min, max to Max. added (float)
           bChan = (float) Math.Round(Math.Min(Math.Max(blue, 0), 255)); //CB Changed round to Round, min to Min, max to Max. added (float)
                   
        }
	}


	private const int MAX = 256;      // max iterations //CB Changed final to const
	private const double SX = -2.025; // start value real  //CB Changed final to const
	private const double SY = -1.125; // start value imaginary //CB Changed final to const
	private const double EX = 0.6;    // end value real //CB Changed final to const
	private const double EY = 1.125;  // end value imaginary //CB Changed final to const
	private static int x1, y1, xs, ys, xe, ye;
	private static double xstart, ystart, xende, yende, xzoom, yzoom;
	private static bool action, rectangle, finished; //CB Changed boolean to bool
	private static float xy;
	private Bitmap picture;
	private Graphics g1;
	private Cursor c1, c2;
	private HSB HSBcol=new HSB();
    Pen myPen1 = new Pen(Color.Black, 1); //CB Moved so accessible for all methods



    public void init() //all instances will be prepared
	{
		HSBcol = new HSB();
        /*this.ClientSize = new System.Drawing.Size(640, 480);//setSize(640, 480);
        this.pictureBox.Size = new System.Drawing.Size(640, 480); */
		finished = false;
		/*addMouseListener(this);
		addMouseMotionListener(this);
        c1 = new Cursor(Cursor.WAIT_CURSOR);
		c2 = new Cursor(Cursor.CROSSHAIR_CURSOR);*/ //CB commented out to try new cursor
        this.Cursor = Cursor.Current; 
        c1 = Cursors.WaitCursor;
        c2 = Cursors.Cross;

		x1 = pictureBox1.Width; //CB Changed getSize().width
        y1 = pictureBox1.Height; //CB Changed getSize().height
		xy = (float)x1 / (float)y1;
		picture = new Bitmap(x1, y1); //CB Changed createImage
        g1 = Graphics.FromImage(picture); //CB Changed picture.getGraphics();
		finished = true;
	}

	public void destroy() // delete all instances 
	{
		if (finished)
		{
			/*removeMouseListener(this);
			removeMouseMotionListener(this);*/ //CB commented out to try and compile
			picture = null;
			g1 = null;
			c1 = null;
			c2 = null;
            GC.Collect();  //CB Changed System.gc(); // garbage collection
		}
	}

	public void start()
	{
		action = false;
		rectangle = false;
		initvalues();
		xzoom = (xende - xstart) / (double)x1;
		yzoom = (yende - ystart) / (double)y1;
		mandelbrot();
	}

	public void stop()
	{
	}
	
	public void paint(Graphics g)
	{
		update(g); 
	}
	
	public void update(Graphics g)
	{
        g.DrawImage(picture, 0, 0); //CB Changed g.drawImage(picture, 0, 0, this);
		if (rectangle)
		{
            Pen myPen = new Pen(Color.White); //CB Changed g.setColor(Color.white);
			if (xs < xe)
			{
                if (ys < ye) g.DrawRectangle(myPen, xs, ys, (xe - xs), (ye - ys)); //CB Changed g.drawRect(xs, ys, (xe - xs), (ye - ys));
                else g.DrawRectangle(myPen, xs, ye, (xe - xs), (ys - ye)); //CB Changed g.drawRect(xs, ye, (xe - xs), (ys - ye));
			}
			else
			{
                if (ys < ye) g.DrawRectangle(myPen, xe, ys, (xs - xe), (ye - ys)); //CB Changed g.drawRect(xe, ys, (xs - xe), (ye - ys));
                else g.DrawRectangle(myPen, xe, ye, (xs - xe), (ys - ye)); //CB Changed g.drawRect(xe, ye, (xs - xe), (ys - ye));
			}
		}
	}
	
	private void mandelbrot() // calculate all points
	{
		int x, y;
		float h, b, alt = 0.0f;
		
		action = false;
        this.Cursor = Cursors.WaitCursor; //CB Changed SetCursor(c1);
		//showStatus("Mandelbrot-Set will be produced - please wait..."); //CB commented out to try and compile
		for (x = 0; x < x1; x+=2)
			for (y = 0; y < y1; y++)
			{
				h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
				if (h != alt)
				{
					b = 1.0f - h * h; // brightnes
					///djm added
					HSBcol.fromHSB(h*255,0.8f*255,b*255); //convert hsb to rgb then make a Java Color ??CB times by 255 to adjust colour
                    Color col = Color.FromArgb((int)HSBcol.rChan, (int)HSBcol.gChan, (int)HSBcol.bChan); //CB Changed Color col = new Color(0,HSBcol.rChan,HSBcol.gChan,HSBcol.bChan);
                    myPen1.Color = col; //CB Set pen colour
                    //g1.setColor(col);
					//djm end
					//djm added to convert to RGB from HSB
					
					/*g1.setColor(Color.getHSBColor(h, 0.8f, b));
					//djm test
				    Color col = Color.getHSBColor(h,0.8f,b);
				    int red = col.getRed();
				    int green = col.getGreen();
				    int blue = col.getBlue(); */ //CB commented out to try and compile
					//djm 
					alt = h;
				}
                
                g1.DrawLine(myPen1, x, y, x + 1, y); //CB Changed g1.drawLine(x, y, x + 1, y);
			}
        //showStatus("Mandelbrot-Set ready - please select zoom area with pressed mouse."); //CB commented out to try and compile
        this.Cursor = Cursors.Cross; //CB Changed setCursor(c2);
		action = true;
	}
	
	private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
	{
		double r = 0.0, i = 0.0, m = 0.0;
		int j = 0;
		
		while ((j < MAX) && (m < 4.0))
		{
			j++;
			m = r * r - i * i;
			i = 2.0 * r * i + ywert;
			r = m + xwert;
		}
		return (float)j / (float)MAX;
	}
	
	private void initvalues() // reset start values
	{
		xstart = SX;
		ystart = SY;
		xende = EX;
		yende = EY;
		if ((float)((xende - xstart) / (yende - ystart)) != xy ) 
			xstart = xende - (yende - ystart) * (double)xy;
	}

    private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
    {
        //e.consume();
        this.Text = "MouseDown"; //CB Changes Window Title to Mouse Event
        if (action)
        {
            xs = e.X; //CB Changed e.getX();
            ys = e.Y; //CB Changed e.getY();
        }
    }

    private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
    {
        int z, w;

        //e.consume();
        this.Text = "MouseUp"; //CB Changes Window Title to Mouse Event
        if (action)
        {
            xe = e.X; //CB Changed e.getX();
            ye = e.Y; //CB Changed e.getY();
            if (xs > xe)
            {
                z = xs;
                xs = xe;
                xe = z;
            }
            if (ys > ye)
            {
                z = ys;
                ys = ye;
                ye = z;
            }
            w = (xe - xs);
            z = (ye - ys);
            if ((w < 2) && (z < 2)) initvalues();
            else
            {
                if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                else xe = (int)((float)xs + (float)z * xy);
                xende = xstart + xzoom * (double)xe;
                yende = ystart + yzoom * (double)ye;
                xstart += xzoom * (double)xs;
                ystart += yzoom * (double)ys;
            }
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
            rectangle = false;
            //repaint(); //CB Commented out to try and compile
            this.Refresh(); //CB added to replace repaint();
        }
    }

    private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
    {

    }

    private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
    {
        //e.consume();
        this.Text = "MouseMove"; //CB Changes Window Title to Mouse Event
        if (e.Button == MouseButtons.Left) //CB added if statement to control when move works
        {
            if (action)
            {
                xe = e.X; //CB Changed e.getX();
                ye = e.Y; //CB Changed e.getY();
                rectangle = true;
                //repaint(); //CB Commented out to try and compile
                this.Refresh();
            }
        }
    }

    public String getAppletInfo()
    {
        return "fractal.class - Mandelbrot Set a Java Applet by Eckhard Roessel 2000-2001";
    }

    private void pictureBox1_Paint(object sender, PaintEventArgs e) //CB added paint method
    {
        paint(e.Graphics);
    }

    private void button1_Click(object sender, EventArgs e) //CB button added to be used to open dialog box for saving image
    {
        SaveFileDialog dialog = new SaveFileDialog(); //CB added to open save dialog box
        dialog.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif"; //CB added to set suitable file types
        if (dialog.ShowDialog() == DialogResult.OK) //CB added to check if save button pressed
        {      
           picture.Save(dialog.FileName); //CB added to save file
        }
    }

    private void button2_Click(object sender, EventArgs e) //CB button used to export the state
    {
        string x1a = x1.ToString(); 
        string y1a = y1.ToString();
        string xstart1 = xstart.ToString();
        string ystart1 = ystart.ToString();
        string xende1 = xende.ToString();
        string yende1 = yende.ToString();
        string xzoom1 = xzoom.ToString();
        string yzoom1 = yzoom.ToString();
        string xy1 = xy.ToString();
        string xs1 = xs.ToString();
        string xe1 = xe.ToString();
        string ys1 = ys.ToString();
        string ye1 = ye.ToString(); //CB converted so can export to text file
        SaveFileDialog exportDialog = new SaveFileDialog(); //CB opened new save dialog
        exportDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
        if (exportDialog.ShowDialog() == DialogResult.OK)
        {
            StreamWriter writer = new StreamWriter(exportDialog.FileName); //CB Created StreamWriter to export data to text file
            writer.WriteLine(x1a);
            writer.WriteLine(y1a);
            writer.WriteLine(xstart1);
            writer.WriteLine(ystart1);
            writer.WriteLine(xende1);
            writer.WriteLine(yende1);
            writer.WriteLine(xzoom1);
            writer.WriteLine(yzoom1);
            writer.WriteLine(xy1);
            writer.WriteLine(xs1);
            writer.WriteLine(xe1);
            writer.WriteLine(ys1);
            writer.WriteLine(ye1); //CB Data written to text file 
            writer.Close();
        }

    }

    private void button3_Click(object sender, EventArgs e) //CB button used to export the state
    {
        OpenFileDialog importDialog = new OpenFileDialog(); //CB Opens File Dialog Box

        importDialog.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*"; //CB Sets filter options

        if (importDialog.ShowDialog() == DialogResult.OK)//CB added to check if open button pressed
        {

            StreamReader reader = new StreamReader(importDialog.FileName); //CB Created StreamReader to import data from text file
            string x1b = reader.ReadLine();
            string y1b = reader.ReadLine();
            string xstart2 = reader.ReadLine();
            string ystart2 = reader.ReadLine();
            string xende2 = reader.ReadLine();
            string yende2 = reader.ReadLine();
            string xzoom2 = reader.ReadLine();
            string yzoom2 = reader.ReadLine();
            string xy2 = reader.ReadLine();
            string xs2 = reader.ReadLine();
            string xe2 = reader.ReadLine();
            string ys2 = reader.ReadLine();
            string ye2 = reader.ReadLine();
            reader.Close();

            x1 = Convert.ToInt32(x1b); //CB Converted text and set variables to import previous state
            y1 = Convert.ToInt32(y1b);
            xstart = Convert.ToDouble(xstart2);
            ystart = Convert.ToDouble(ystart2);
            xende = Convert.ToDouble(xende2);
            yende = Convert.ToDouble(yende2);
            xzoom = Convert.ToDouble(xzoom2);
            yzoom = Convert.ToDouble(yzoom2);
            xy = Convert.ToSingle(xy2);
            xs = Convert.ToInt32(xs2);
            xe = Convert.ToInt32(xe2);
            ys = Convert.ToInt32(ys2);
            ye = Convert.ToInt32(ye2);
            mandelbrot(); //CB call mandelbrot method
            this.Refresh(); //CB Refresh the Form
        }
    }



    }
}